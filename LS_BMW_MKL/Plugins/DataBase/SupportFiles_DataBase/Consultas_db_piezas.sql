--trae todas las piezas en general 
Select p.QR as qr, p.fecha_produccion as f_produccion,  p.fecha_registro as f_registro, p.DSN as dsn, c.QR as caja from tbl_pieza as p 
INNER JOIN tbl_caja as c on c.id_caja = p.fk_caja;

--Trae todas las cajas en general 
Select * from tbl_caja;


--Trae piezas de una caja en especifico 
Select p.QR as qr, p.fecha_produccion as f_produccion,  p.fecha_registro as f_registro, p.DSN as dsn, c.QR as caja from tbl_pieza as p 
INNER JOIN tbl_caja as c on c.id_caja = p.fk_caja 
Where c.id_caja = 1;

--Trae una caja en especifico 
Select * from tbl_caja where id_caja = 1;

--Trae una pieza en especifico
Select p.QR as qr, p.fecha_produccion as f_produccion,  p.fecha_registro as f_registro, p.DSN as dsn, c.QR as caja from tbl_pieza as p 
INNER JOIN tbl_caja as c on c.id_caja = p.fk_caja 
Where p.id_pieza = 1;

--Traer pieza  en fecha especifica 
Select p.QR as qr, p.fecha_produccion as f_produccion,  p.fecha_registro as f_registro, p.DSN as dsn, c.QR as caja from tbl_pieza as p 
INNER JOIN tbl_caja as c on c.id_caja = p.fk_caja 
Where p.fecha_registro BETWEEN '2019-11-01 00:00:00' and '2019-11-01 23:59:59';


--Traer caja en fecha especifica 
Select QR,  fecha_registro, turno  from tbl_caja  
Where fecha_registro BETWEEN '2019-11-01 00:00:00' and  '2019-11-01 23:59:59';

