CREATE TABLE tbl_caja (
  id_caja int NOT NULL IDENTITY(1,1) PRIMARY KEY,
  fecha_produccion datetime NOT NULL DEFAULT current_timestamp,
  serial_box varchar(128) NOT NULL,
  turno int NOT NULL,
  status int NOT NULL DEFAULT 1
)

--
-- Volcado de datos para la tabla `tbl_caja`
--

INSERT INTO tbl_caja ( fecha_produccion, serial_box, turno, status) VALUES
( '2019-11-01 11:07:37', 'BHTC011119D1344341S01H110737N0001', 1, 1),
( '2019-11-01 12:08:26', 'BHTC011119D1344341S01H120826N0002', 1, 1),
( '2019-11-01 14:08:26', 'BHTC011119D1344341S02H140826N0003', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_pieza`
--

CREATE TABLE tbl_pieza (
  id_pieza int NOT NULL IDENTITY(1,1) PRIMARY KEY,
  Serial_Number varchar(128) NOT NULL,
  BHTC_Device# varchar(128) NOT NULL,
  Customer_Device# varchar(128) NOT NULL,
  fecha_produccion datetime NOT NULL,
  fecha_escaneo datetime NOT NULL DEFAULT current_timestamp,
  DSN int NOT NULL,
  turno int NOT NULL,
  serial_box varchar(128) NOT NULL,
  status int NOT NULL DEFAULT 1
) 
--
-- Volcado de datos para la tabla `tbl_pieza`
--

INSERT INTO tbl_pieza (Serial_Number, BHTC_Device#, Customer_Device#, fecha_produccion, fecha_escaneo, DSN, turno, serial_box, status) VALUES
('YRA-00101.11.1903150001', '1344341', '2G0907057E','2019-11-01 10:00:23', '2019-11-01 10:05:51', 0368, 1,'BHTC011119D1344341S01H110737N0001',1),
('YRA-00101.11.1903150025', '1344336', '2G0907057E','2019-11-01 11:23:45', '2019-11-01 11:24:51', 0456, 1,'BHTC011119D1344341S01H120826N0002',1),
('YRA-00101.11.1903150036', '1344341', '2G0907057E','2019-11-01 15:42:17', '2019-11-01 15:44:08', 0574, 2,'BHTC011119D1344341S02H140826N0003',1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_caja`
--

--
-- Filtros para la tabla `tbl_pieza`
--
ALTER TABLE tbl_pieza
  ADD CONSTRAINT tbl_pieza_ibfk_1 FOREIGN KEY (fk_caja) REFERENCES tbl_caja (id_caja);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
