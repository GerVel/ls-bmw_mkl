﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Framework Support VIs" Type="Folder">
			<Item Name="Operation Modes" Type="Folder">
				<Item Name="Production Mode" Type="Folder">
					<Item Name="Produccion sendQueue(Array).vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Produccion sendQueue(Array).vi"/>
					<Item Name="Produccion Preloop.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Produccion Preloop.vi"/>
					<Item Name="Produccion sendQueue(Pol).vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Produccion sendQueue(Pol).vi"/>
					<Item Name="Preserved System Vars.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Vars/Preserved System Vars.vi"/>
					<Item Name="Perserved Production Vars.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Perserved Production Vars.vi"/>
					<Item Name="Check for day change.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Check for day change.vi"/>
					<Item Name="Complete Device_Serial Number.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Complete Device_Serial Number.vi"/>
					<Item Name="Add to Issues List.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Add to Issues List.vi"/>
					<Item Name="Get and compare Customer Number.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Get and compare Customer Number.vi"/>
					<Item Name="Production Mode v2.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Production Mode v2.vi"/>
					<Item Name="Count Devices on Box.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Count Devices on Box.vi"/>
					<Item Name="Increment boxes.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Increment boxes.vi"/>
					<Item Name="Generate ZPL Vars.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Generate ZPL Vars.vi"/>
					<Item Name="Increment daily count boxes.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Increment daily count boxes.vi"/>
					<Item Name="Prouduccion Queue FGV.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Prouduccion Queue FGV.vi"/>
					<Item Name="Produccion sendQueue.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Produccion sendQueue.vi"/>
					<Item Name="Produccion deQueue.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Produccion deQueue.vi"/>
					<Item Name="Check If Devices Exist on DB..vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Check If Devices Exist on DB..vi"/>
					<Item Name="Add to Devices List.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Add to Devices List.vi"/>
					<Item Name="Wait for DB register confirmation..vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Wait for DB register confirmation..vi"/>
					<Item Name="Complete Device data.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Complete Device data.vi"/>
					<Item Name="Pieza Buffer to complete.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Pieza Buffer to complete.vi"/>
					<Item Name="string to date.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/string to date.vi"/>
					<Item Name="To 4 digits number string.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/To 4 digits number string.vi"/>
					<Item Name="Simulate Scanner Send Data.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Simulate Scanner Send Data.vi"/>
					<Item Name="simulate Scanner Send Data_BMW.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/simulate Scanner Send Data_BMW.vi"/>
					<Item Name="Record Box on DB.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Produccion Mode/Record Box on DB.vi"/>
				</Item>
				<Item Name="Consult Box Mode" Type="Folder">
					<Item Name="Get Box &amp; Devices.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Box Mode/Get Box &amp; Devices.vi"/>
					<Item Name="Complete Box Info.ctl" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Box Mode/Complete Box Info.ctl"/>
					<Item Name="ConsultBox deQueue.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Box Mode/ConsultBox deQueue.vi"/>
					<Item Name="ConsultBox Preloop.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Box Mode/ConsultBox Preloop.vi"/>
					<Item Name="ConsultBox Queue FGV.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Box Mode/ConsultBox Queue FGV.vi"/>
					<Item Name="ConsultBox sendQueue(Array).vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Box Mode/ConsultBox sendQueue(Array).vi"/>
					<Item Name="ConsultBox sendQueue.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Box Mode/ConsultBox sendQueue.vi"/>
					<Item Name="Compare last Serial Box.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Box Mode/Compare last Serial Box.vi"/>
					<Item Name="Consult Box Mode.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Box Mode/Consult Box Mode.vi"/>
					<Item Name="Check space on box.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Box Mode/Check space on box.vi"/>
				</Item>
				<Item Name="Consult Device Serial Mode" Type="Folder">
					<Item Name="Consult DS Queue FGV.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Device Serial Mode/Consult DS Queue FGV.vi"/>
					<Item Name="Consult DS sendQueue.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Device Serial Mode/Consult DS sendQueue.vi"/>
					<Item Name="Data to Text Info.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Device Serial Mode/Data to Text Info.vi"/>
					<Item Name="Mail Request.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Device Serial Mode/Mail Request.vi"/>
					<Item Name="Consult DS deQueue.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Device Serial Mode/Consult DS deQueue.vi"/>
					<Item Name="Consult DS Preloop.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Device Serial Mode/Consult DS Preloop.vi"/>
					<Item Name="Consult Device Serial Mode.vi" Type="VI" URL="../Framework/Framework Support VIs/Modes/Consult Device Serial Mode/Consult Device Serial Mode.vi"/>
				</Item>
			</Item>
			<Item Name="General" Type="Folder">
				<Item Name="Manage Panel Controls.vi" Type="VI" URL="../Framework/Framework Support VIs/Manage Panel Controls.vi"/>
				<Item Name="Get Configuration.vi" Type="VI" URL="../Framework/Framework Support VIs/Get Configuration.vi"/>
				<Item Name="Init UI.vi" Type="VI" URL="../Framework/Framework Support VIs/Init UI.vi"/>
				<Item Name="Get Generic values from config.vi" Type="VI" URL="../Framework/Framework Support VIs/Get Generic values from config.vi"/>
				<Item Name="Get Operation refs from config.vi" Type="VI" URL="../Framework/Framework Support VIs/Get Operation refs from config.vi"/>
				<Item Name="Get Device#s form config.vi" Type="VI" URL="../Framework/Framework Support VIs/Get Device#s form config.vi"/>
				<Item Name="Get Mail options form config.vi" Type="VI" URL="../Framework/Framework Support VIs/Get Mail options form config.vi"/>
				<Item Name="Check Warnings.vi" Type="VI" URL="../support/Check Warnings.vi"/>
				<Item Name="Refresh User Controls.vi" Type="VI" URL="../Framework/Framework Support VIs/Refresh User Controls.vi"/>
				<Item Name="Send exit msg to Plugins.vi" Type="VI" URL="../Framework/Framework Support VIs/Send exit msg to Plugins.vi"/>
				<Item Name="Display temporal Msg.vi" Type="VI" URL="../Framework/Framework Support VIs/Display temporal Msg.vi"/>
				<Item Name="Blink Status.vi" Type="VI" URL="../Framework/Framework Support VIs/Blink Status.vi"/>
				<Item Name="Modules User Events FGV.vi" Type="VI" URL="../Framework/Framework Support VIs/Modules User Events FGV.vi"/>
				<Item Name="Select Operation.vi" Type="VI" URL="../Framework/Framework Support VIs/Select Operation.vi"/>
			</Item>
			<Item Name="System Tools" Type="Folder">
				<Item Name="Manual Count.vi" Type="VI" URL="../Framework/Framework Support VIs/Manual Count.vi"/>
				<Item Name="Shift 1 count.vi" Type="VI" URL="../Framework/Framework Support VIs/Shift 1 count.vi"/>
				<Item Name="Shift 2 count.vi" Type="VI" URL="../Framework/Framework Support VIs/Shift 2 count.vi"/>
				<Item Name="Shift 3 count.vi" Type="VI" URL="../Framework/Framework Support VIs/Shift 3 count.vi"/>
				<Item Name="System tools Queue FGV.vi" Type="VI" URL="../Framework/Framework Support VIs/Queue FGV/System tools Queue FGV.vi"/>
				<Item Name="System tools sendQueue.vi" Type="VI" URL="../Framework/Framework Support VIs/Queue FGV/System tools sendQueue.vi"/>
				<Item Name="System tools deQueue.vi" Type="VI" URL="../Framework/Framework Support VIs/Queue FGV/System tools deQueue.vi"/>
				<Item Name="Send Mail Notification.vi" Type="VI" URL="../Framework/Framework Support VIs/Send Mail Notification.vi"/>
			</Item>
		</Item>
		<Item Name="Plugins" Type="Folder">
			<Item Name="Templates" Type="Folder">
				<Item Name="Puglin Hanlder.vit" Type="VI" URL="../Plugins/Plugin Templates/Puglin Hanlder.vit"/>
				<Item Name="Plugin deQueue.vit" Type="VI" URL="../Plugins/Plugin Templates/Plugin deQueue.vit"/>
				<Item Name="Plugin Queue FGV.vit" Type="VI" URL="../Plugins/Plugin Templates/Plugin Queue FGV.vit"/>
				<Item Name="Plugin sendQueue(Array).vit" Type="VI" URL="../Plugins/Plugin Templates/Plugin sendQueue(Array).vit"/>
				<Item Name="Plugin sendQueue(Pol).vit" Type="VI" URL="../Plugins/Plugin Templates/Plugin sendQueue(Pol).vit"/>
				<Item Name="Plugin sendQueue.vit" Type="VI" URL="../Plugins/Plugin Templates/Plugin sendQueue.vit"/>
			</Item>
			<Item Name="Support VIs" Type="Folder">
				<Item Name="Buffers_SEQ" Type="Folder">
					<Item Name="BufferPiezas_Create.vi" Type="VI" URL="../Plugins/DataBase/suppot_vis/Buffers_SEQ/BufferPiezas_Create.vi"/>
					<Item Name="BufferPiezas_Get.vi" Type="VI" URL="../Plugins/DataBase/suppot_vis/Buffers_SEQ/BufferPiezas_Get.vi"/>
					<Item Name="bufferPiezas_Set.vi" Type="VI" URL="../Plugins/DataBase/suppot_vis/Buffers_SEQ/bufferPiezas_Set.vi"/>
				</Item>
				<Item Name="NotifiersTriggersMonitoring.vi" Type="VI" URL="../Plugins/Support VIs/NotifiersTriggersMonitoring.vi"/>
			</Item>
			<Item Name="DataBase.lvclass" Type="LVClass" URL="../Plugins/DataBase/DataBase.lvclass"/>
			<Item Name="Scanner.lvclass" Type="LVClass" URL="../Plugins/Scanner/Scanner.lvclass"/>
			<Item Name="Printer.lvclass" Type="LVClass" URL="../Plugins/Printer/Printer.lvclass"/>
		</Item>
		<Item Name="Support Libraries &amp; VIs" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="UI Data SEQ" Type="Folder">
				<Item Name="ui_create.vi" Type="VI" URL="../Framework/Framework Support VIs/UI Data SEQ/ui_create.vi"/>
				<Item Name="ui_get.vi" Type="VI" URL="../Framework/Framework Support VIs/UI Data SEQ/ui_get.vi"/>
				<Item Name="ui_set.vi" Type="VI" URL="../Framework/Framework Support VIs/UI Data SEQ/ui_set.vi"/>
			</Item>
			<Item Name="Queue FGV" Type="Folder">
				<Item Name="Framework Queue FGV.vi" Type="VI" URL="../Framework/Framework Support VIs/Queue FGV/Framework Queue FGV.vi"/>
				<Item Name="Framework deQueue.vi" Type="VI" URL="../Framework/Framework Support VIs/Queue FGV/Framework deQueue.vi"/>
				<Item Name="Framework preLoop.vi" Type="VI" URL="../Framework/Framework Support VIs/Queue FGV/Framework preLoop.vi"/>
				<Item Name="Framework sendQueue(Array).vi" Type="VI" URL="../Framework/Framework Support VIs/Queue FGV/Framework sendQueue(Array).vi"/>
				<Item Name="Framework sendQueue(Pol).vi" Type="VI" URL="../Framework/Framework Support VIs/Queue FGV/Framework sendQueue(Pol).vi"/>
				<Item Name="Framework sendQueue.vi" Type="VI" URL="../Framework/Framework Support VIs/Queue FGV/Framework sendQueue.vi"/>
			</Item>
			<Item Name="Send Mail" Type="Folder">
				<Item Name="SubVIs" Type="Folder">
					<Item Name="Configure Sender and Receiver.vi" Type="VI" URL="../support/Send Mail/SubVIs/Configure Sender and Receiver.vi"/>
					<Item Name="Configure Server and Send Email.vi" Type="VI" URL="../support/Send Mail/SubVIs/Configure Server and Send Email.vi"/>
					<Item Name="Write Email Body.vi" Type="VI" URL="../support/Send Mail/SubVIs/Write Email Body.vi"/>
					<Item Name="Mail Server Options.ctl" Type="VI" URL="../support/Send Mail/Mail Server Options.ctl"/>
					<Item Name="Delete previous files from temp folder.vi" Type="VI" URL="../support/Send Mail/SubVIs/Delete previous files from temp folder.vi"/>
				</Item>
				<Item Name="Send Gmail.vi" Type="VI" URL="../support/Send Mail/Send Gmail.vi"/>
				<Item Name="Mail Message.ctl" Type="VI" URL="../support/Send Mail/Mail Message.ctl"/>
				<Item Name="Mail Content.ctl" Type="VI" URL="../support/Send Mail/Mail Content.ctl"/>
			</Item>
			<Item Name="Message Queue.lvlib" Type="Library" URL="../support/Message Queue/Message Queue.lvlib"/>
			<Item Name="User Event - Stop.lvlib" Type="Library" URL="../support/User Event - Stop/User Event - Stop.lvlib"/>
			<Item Name="Printer ZPL Driver.lvlib" Type="Library" URL="../support/Printer ZPL/Printer ZPL Driver.lvlib"/>
			<Item Name="Testing_MX_Methods.lvlib" Type="Library" URL="../support/TestingMX/Testing_MX_Methods.lvlib"/>
			<Item Name="Check Loop Error.vi" Type="VI" URL="../support/Check Loop Error.vi"/>
			<Item Name="Error Handler - Event Handling Loop.vi" Type="VI" URL="../support/Error Handler - Event Handling Loop.vi"/>
			<Item Name="Error Handler - Message Handling Loop.vi" Type="VI" URL="../support/Error Handler - Message Handling Loop.vi"/>
			<Item Name="Check Notifiers Response(without enclaving).vi" Type="VI" URL="../support/Check Notifiers Response(without enclaving).vi"/>
			<Item Name="Check Notifiers Response.vi" Type="VI" URL="../support/Check Notifiers Response.vi"/>
			<Item Name="Extract Error From Variant Message.vi" Type="VI" URL="../Framework/Framework Support VIs/Extract Error From Variant Message.vi"/>
			<Item Name="Check for Plugins Exit.vi" Type="VI" URL="../support/Message Queue/Check for Plugins Exit.vi"/>
			<Item Name="Read Plugins Status.vi" Type="VI" URL="../Framework/Framework Support VIs/Read Plugins Status.vi"/>
			<Item Name="IDdb.vi" Type="VI" URL="../support/IDdb.vi"/>
			<Item Name="GetID.vi" Type="VI" URL="../support/GetID.vi"/>
			<Item Name="Waiting Plugins.vi" Type="VI" URL="../Framework/Framework Support VIs/Waiting Plugins.vi"/>
			<Item Name="DataBaseSendQuerry.vi" Type="VI" URL="../support/DataBaseSendQuerry.vi"/>
			<Item Name="ReciDataQuerry.vi" Type="VI" URL="../support/ReciDataQuerry.vi"/>
			<Item Name="CountDiveces.vi" Type="VI" URL="../support/CountDiveces.vi"/>
			<Item Name="Extract Warning From Variant Message.vi" Type="VI" URL="../Framework/Framework Support VIs/Extract Warning From Variant Message.vi"/>
		</Item>
		<Item Name="Type Definitions" Type="Folder">
			<Item Name="Configurations.ctl" Type="VI" URL="../controls/Configurations.ctl"/>
			<Item Name="Operation.ctl" Type="VI" URL="../controls/Operation.ctl"/>
			<Item Name="Panel Operation Refs.ctl" Type="VI" URL="../controls/Panel Operation Refs.ctl"/>
			<Item Name="UI Controls.ctl" Type="VI" URL="../controls/UI Controls.ctl"/>
			<Item Name="Plugins Response.ctl" Type="VI" URL="../support/User Event - Stop/FGV_Notifiers/Plugins Response.ctl"/>
			<Item Name="UI Data.ctl" Type="VI" URL="../controls/UI Data.ctl"/>
			<Item Name="Action.ctl" Type="VI" URL="../support/User Event - Stop/FGV_User Event/Action.ctl"/>
		</Item>
		<Item Name="!Framework.vi" Type="VI" URL="../!Framework.vi"/>
		<Item Name="Icon.ico" Type="Document" URL="../Icon.ico"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GOOP Object Repository Method.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Method.ctl"/>
				<Item Name="GOOP Object Repository Statistics.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Statistics.ctl"/>
				<Item Name="GOOP Object Repository.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_Database_API.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/database/NI_Database_API.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="ex_BuildTextVarProps.ctl" Type="VI" URL="/&lt;vilib&gt;/express/express output/BuildTextBlock.llb/ex_BuildTextVarProps.ctl"/>
			</Item>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="LayerScan.exe" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{51D186D8-2FD9-4F4B-8F36-40D68E57FFBC}</Property>
				<Property Name="App_INI_GUID" Type="Str">{F8BAE070-8383-465E-800B-3DF9D752A65B}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{96FCFFEE-92FF-41D7-8A5C-2E1B7D4F08B1}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">LayerScan.exe</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{9236DFC5-A1BE-45C5-93F4-A12737594CB8}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">LayerScan.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/LayerScan.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/Icon.ico</Property>
				<Property Name="Source[0].itemID" Type="Str">{069DB0C3-8ACD-4013-B1E7-A0C6EF3032B0}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/!Framework.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/Modo_Pieza_Consulta.vi</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/Production Mode.vi</Property>
				<Property Name="Source[11].type" Type="Str">VI</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/Read Current Boxes.vi</Property>
				<Property Name="Source[12].type" Type="Str">VI</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/RegistrarPieza_Produccion.vi</Property>
				<Property Name="Source[13].type" Type="Str">VI</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/Write Current Boxes.vi</Property>
				<Property Name="Source[14].type" Type="Str">VI</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/Plugins/Scanner.lvclass/Read.vi</Property>
				<Property Name="Source[15].type" Type="Str">VI</Property>
				<Property Name="Source[16].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[16].itemID" Type="Ref">/My Computer/Plugins/Scanner.lvclass/Scanner Engine.vi</Property>
				<Property Name="Source[16].type" Type="Str">VI</Property>
				<Property Name="Source[17].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[17].itemID" Type="Ref">/My Computer/Plugins/Scanner.lvclass/Write.vi</Property>
				<Property Name="Source[17].type" Type="Str">VI</Property>
				<Property Name="Source[18].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[18].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/Functions/Consultar_Caja.vi</Property>
				<Property Name="Source[18].type" Type="Str">VI</Property>
				<Property Name="Source[19].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[19].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/Functions/Consultar_Pieza.vi</Property>
				<Property Name="Source[19].type" Type="Str">VI</Property>
				<Property Name="Source[2].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Framework Support VIs</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[20].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[20].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/Functions/Registrar_Caja.vi</Property>
				<Property Name="Source[20].type" Type="Str">VI</Property>
				<Property Name="Source[21].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[21].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/Functions/Registrar_Pieza.vi</Property>
				<Property Name="Source[21].type" Type="Str">VI</Property>
				<Property Name="Source[22].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[22].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[22].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[22].itemID" Type="Ref">/My Computer/Plugins</Property>
				<Property Name="Source[22].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[22].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[3].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Support Libraries &amp; VIs</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[4].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Type Definitions</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass</Property>
				<Property Name="Source[5].type" Type="Str">Library</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Plugins/Scanner.lvclass</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/Data Base Engine.vi</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/InterrupcionOK.vi</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/Plugins/DataBase.lvclass/Modo_Caja_Consulta.vi</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">23</Property>
				<Property Name="TgtF_fileDescription" Type="Str">LayerScan.exe</Property>
				<Property Name="TgtF_internalName" Type="Str">LayerScan.exe</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2012 </Property>
				<Property Name="TgtF_productName" Type="Str">LayerScan.exe</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{8D31CF1E-BFEE-4FAB-AC90-991853A95B09}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">LayerScan.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
